// home
if ( document.getElementsByClassName('homepage').length ) {
    const tl = gsap.timeline();
    tl.to('.homepage .main-logo', 0.8, {opacity: 1});
    tl.to('.homepage .main-logo', 0.8, {scaleX: 1.5, scaleY: 1.5, ease: Back.easeInOut }, 1);
    tl.to('.homepage .main-logo', 0.8, {scaleX: 1, scaleY: 1, y: 0, x:0, ease: Back.easeInOut }, 3);
    tl.to('.intro-mask', 0.8, { opacity: 0, onComplete: function() {
        document.getElementsByClassName('intro-mask')[0].style.display = 'none';
        slogan_anim.play();
    }}, 3.6);
    tl.from('.homepage .video', 0.5, { y: 50 }, 3.6);
    tl.to('.homepage .main-logo__slogan', 0.5, { opacity: 1}, 4);
}

// video
const video_thumbs = document.getElementsByClassName('video__thumb');

for ( var i = 0; i < video_thumbs.length; i++ ) {
    video_thumbs[i].onclick = (e) => {
        e.target.parentElement.innerHTML = '<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/373133556?autoplay=1&title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>';
    }
}

// show modal
// const show_modal = document.querySelector('#register-form.register-page');
let thanks_modal = document.getElementById('thanks_modal');

if ( thanks_modal ) {
    setTimeout( () => {
        thanks_modal = document.getElementById('thanks_modal');

        if ( thanks_modal ) {
            document.body.classList.add('no_scroll');
            thanks_modal.classList.add('show');
        }
    }, 1000);

    const modal_button = thanks_modal.querySelector('.modal__button');

    modal_button.addEventListener('click', () => {
        document.body.classList.remove('no_scroll');
        thanks_modal.classList.remove('show');
        thanks_modal.classList.add('hide');
        setTimeout( () => { thanks_modal.style.display = 'none'; }, 500 );    
    });
}

// register animation
if ( document.getElementById('main-content').classList.contains('register-page') ) {
    const ra_timeline = new TimelineMax({onUpdate:updateRegisterAnimation});
    const ra_controller = new ScrollMagic.Controller();
    
    ra_timeline.to('.register-page .second-column .video', 1, { y: 200, opacity: 0 });
    // ra_timeline.from('#register-form .box__intro', 1, {y: 20, opacity:0}, 0);
    ra_timeline.from('#register-form .box__title', 1, {marginBottom: 80}, 0);
    ra_timeline.to('#register-form .form__container', 1, {opacity:1}, 0);
    
    const ra_scene = new ScrollMagic.Scene({
        triggerElement: '#register-form',
        triggerHook: 0.70,
        duration: '50%'
    })
        .setTween( ra_timeline )
        .addTo( ra_controller );
    
    function updateRegisterAnimation() {
        ra_timeline.progress();
    }
}

// register button
if ( document.getElementById('register-form') ) {
    const inputs = document.getElementById('register-form').getElementsByTagName('input');

    for ( var i = 0; i < inputs.length; i++ ) {
        if ( inputs[i].value ) {
             inputs[i].classList.add('filled');
        }

        inputs[i].onkeyup = (e) => {
            console.log(typeof isEmpty);
            if ( e.target.classList.contains('error') ) {
                e.target.classList.remove('error');
                e.target.parentNode.querySelector('label.error').remove();
            }

            if ( e.target.value ) {
                e.target.classList.add('filled');
            } else {
                e.target.classList.remove('filled');
            }
        }
    }

    document.querySelector('#register-form .form__button button').onclick = () => {
        document.querySelector('#register-form .form__button').classList.add('loading');
    };
}